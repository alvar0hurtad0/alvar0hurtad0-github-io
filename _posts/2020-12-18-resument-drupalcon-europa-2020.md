---
layout: post
title:  "Mi resumen de la Drupalcon europa 2020"
author: alvaro
categories: [ drupal ]
image: assets/images/drupalcon-2020.png
image_credits: https://alvar0hurtad0.es
description: "Este año está siendo todo diferente y la drupalcon no es una excepción. Se ha celebrado de forma virtual, a continuación resumo mis impresiones."
featured: false
hidden: false
---
La drupalcon europea de este año estaba previsto que se celebrara en Barcelona. Cuando se anunció hace un año me hizo mucha ilusión, porque tenía ganas de volver a ver a mucha gente de la comunidad y tenía la sensación de que este año que me siento mucho más seguro con el inglés iba a ser una gran oportunidad para disfrutar mucho más del evento más importante a nivel mundial relacionado con Drupal.

Entonces llegó la pandemia y todo se quedó en el aire. Se ha hecho de forma virtual, en remoto, con sesiones de Zoom y chats de una plataforma especializada en eventos virtuales. Al cambiar tanto el formato, la experiencia no ha sido igual. No puedo decir que haya sido ni mejor ni peor, solo diferente.

## Cosas que no me han gustado de este formato
Al estar de drupalcon en tu mesa y tu silla, con el chat del trabajo abierto, no desconectas realmente y terminas estando de trabajo pero con algunos cortes en los que hay alguna charla que te interesa.

Es mucho más complicado hacer networking. Había un sistema de intercambio de tarjetas y un sistema de contactos para hablar, pero no es lo mismo. Si quieres conocer a la gente que está manteniendo un determinado módulo, las drupalcon presenciales son una gran oportunidad, porque puedes comer o dar un paseo con la persona que sea y realmente conocerla. En formato virtual no hay ninguna que no se dé ya con los múltiples canales de comunicación que hay normalmente.

Las charlas eran mucho más frías. Yo no quise ni tan siquiera proponer charla porque la experiencia como ponente de ver las caras de la gente, escuchar sus risas y tener toda la información del lenguaje no verbal que consigues cuando estás dando una charla, el lo que realmente me hace disfrutar en esos momentos. Explicar unas slides a la pantalla de tu ordenador me parece una experiencia mucho menos gratificante que, en mi caso no haría que mereciera la pena todo el trabajo que conlleva preparar una charla.

## Cosas que si me han gustado del formato on-line

No he tenido que irme una semana de mi casa, lejos de mi familia.

Ver una charla grabada es mucho más eficiente que verla en directo. Las charlas quedaban grabadas y accesibles inmediatamente después de realizarse, y la verdad es que me he visto unas cuantas durante la drupalcon, pero por la noche. Poder pasar la parte de "Soy XXXX y trabajo en XXXX", para poder ir al grano hace que tardes menos en verlas. Desgraciadamente no estaba la opción de reproducir a más velocidad (me encanta esa opción).

No me he pillado la famosa drupalFlu. No falla, el fin de semana después de todas las drupalcon siempre me he puesto malo. Espero que en las futuras podamos aplicar todo lo aprendido durante esta pandemia para que no vuelva a pasar.

Siento que he apoyado a la comunidad. Realmente podría haber esperado unas semanas para ver las charlas, que serán públicas pronto, pero creo que el haber asistido tiene más que ver con apoyar la comunidad que cono el beneficio real que hemos tenido los asistentes. No solo porque el dinero de la entrada le viene bien a la Drupal Association, es porque la comunidad Drupal sigue siendo una comunidad unida y ha quedado claro que ni una pandemia puede evitar que sigamos haciendo nuestros eventos aunque sea en remoto.

