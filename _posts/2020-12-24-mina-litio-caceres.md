---
layout: post
title:  "Por qué estoy en contra de la mina de litio de Cáceres"
author: alvaro
categories: [ Personal ]
image: assets/images/mina-cielo-abierto.jpg
image_credits: https://unsplash.com/photos/h0l3h5vMpk4
description: "Estoy en contra de la mina de litio que se quiere explotar en Cáceres, a continuación cuento por qué"
featured: false
hidden: false
---

No me gusta dar mi opinión sobre temas polémicos o que pueden dar pie a discusiones irracionales, pero en este caso lo voy a hacer. Me gustaría explicar por qué estoy en contra de que se empiece con la explotación de la mina de litio que hay proyectada dentro del término municipal de Cáceres.

Como contexto, para quien no me conozca, me considero una persona que ha colaborado activamente en el desarrollo económico de Cáceres. He llevado a cabo diversas actividades empresariales con las que he tenido la suerte de ayudar en la creación de empleo (varias decenas de puestos de trabajo) y pagar varios cientos de miles de euros en impuestos en nuestra tierra. Actualmente estoy pasando una época en la que no estoy llevando a cabo ningún proyecto relevante de emprendimiento, porque mi mayor proyecto actualmente es mi familia y estar con mis hijos todo el tiempo posible, pero volveré a emprender cuando lo considere oportuno. Con esto solo quiero dejar claro que para mi es muy importante el desarrollo económico de mi tierra. Por si alguien puede pensar que soy una persona acomodada a la que no le importa lo complicado que es quedarse a trabajar aquí. Es un tema que no solo me preocupa, sino en el que además considero que he luchado mucho para aportar mi granito de arena.

Volvamos al tema de la mina, que es el relevante en este artículo. Me gustaría dar mi opinión desde varios puntos de vista:

## No a la mina desde el punto de vista económico.
Para resumir, creo el impacto económico que va a suponer para Cáceres y para Extremadura es casi irrelevante.

### La información que nos dan para que lo veamos como una gran oportunidad económica son engañosas:
Es fácil encontrar titulares de este estilo
> La empresa de la mina de litio ofrece 60 millones al Ayuntamiento de Cáceres para inversiones

Suponiendo que se trate de dinero que le van a dar directamente al ayuntamiento y no inversiones que se vayan a hacer para explotar la mina y obtener beneficio, están hablando de el dinero que van a invertir a lo largo de los 30 años que dura la explotación. Es decir, si dividimos 60 millones entre 30 años y el resultado entre los 100.000 habitantes que tiene la ciudad tenemos una inversión por habitante de 1€ con 67 céntimos al mes (redondeando al alza).

### El dinero generado se irá a fuera de Extremadura
Hay dos grandes creencias que pueden llevar a engaño en este asunto, el primero es que se ha creado un consorcio de empresas llamado Tecnología Extremeña del Litio. Que tiene un nombre muy de la zona, pero tiene sede social en Madrid. Y a pesar de que pueda tener la sede fiscal en Extremadura, eso no significa que vaya a pagar aquí impuestos. Los consorcios son uniones de empresas que pueden tener la contabilidad independiente de sus integrantes o pueden no tenerla. En el caso de no tener la contabilidad independiente de sus integrantes, cada socio llevará su propia contabilidad y atenderá sus obligaciones fiscales donde corresponda.

El segundo es que los puestos de trabajo que se creen no tienen por qué ser en Extremadura. Está claro que alguien tiene que manejar las máquinas que estén metidas en el hoyo. Pero los puestos administrativos pueden realizarse perfectamente desde cualquier otro centro de trabajo en cualquier otra ciudad.

### El mayor negocio no está en la extracción del mineral sino en la fabricación de baterías
Dejando a un lado que la explotación de la mina no tiene por qué dejar el dinero en Extremadura, el eslabón de la cadena de valor donde mayor revalorización se produce es cuando las materias primas se ensablan, se les pone un logotipo de homologación y se convierten en un producto de consumo. Eso ocurrirá lejos de aquí.

A pesar de que se ha anunciado la posible instalación de una fábrica de baterías en Badajoz, creo que se trata de una trampa. Me parece un primer paso en el que se está utilizando la rivalidad que pudiera existir entre las dos capitales extremeñas para que se acepte la mina dentro de los cacereños. Me explico: Primero se comunica que se quiere hacer una mina en Cáceres y una fábrica de baterías en Badajoz. De esta manera los cacereños entramos en cólera, porque vemos que en Cáceres se va a crear poco empleo y de mala calidad mientras que en Badajoz se va a aprovechar el mineral que han sacado los cacereños metiéndose en una ciénaga contaminada, para que se creen más puestos de trabajo, a cubierto y con muchos menos riesgos para la salud. El segundo paso consiste en que el grupo político que acepte el proyecto de la mina anuncie que la fábrica de baterías se hará en Cáceres, de manera que nos tendremos que comer la mina igualmente, pero por lo menos la fábrica de baterías no se monta en Badajoz. De esta manera los cácereños nos quedamos jodidos pero contentos.

Este último escenario si que tendría algo más de repercusión económica para la ciudad. A pesar de ello creo que no sería suficientemente importante como para que compense el impacto para el medio ambiente y para la salud de los cacereños que me temo que tendremos.

## No a la mina desde el punto de vista de un ciudadano de Cáceres
Como he comentado al principio, el empleo en Cáceres es algo por lo que hay que luchar. Pero creo que no a cualquier precio. Hay decenas de iniciativas que me parecen mucho más efectivas a nivel de creación de empleo que además contribuyen a la mejora de la ciudad en otros indicadores como la cultura y la diversidad.

Puedo poner ejemplos como la Fundación Helga de Alvear que ha colocado a Cáceres en el mapa de las ciudades más relevantes del mundo del arte contemporáneo. Y ojo, que yo no soy un aficionado al arte y realmente lo valoro poco, pero ha dotado a Cáceres una dimensión nueva sobre la que crecer económicamente como es el turismo de lujo y las galerías de arte que están floreciendo por las inmediaciones.

Otros ejemplos que conozco de cerca son las empresas de desarrollo de software que están implantando factorías en nuestra ciudad y crean empleo muy bien pagado y sin necesidad de contaminar o hacer un agujero gigantesco en la sierra.

Podría poner otros muchos ejemplos, como el Centro Budista, el Centro de Cirugía de Mínima Invasión Jesús Usón, o los parques solares que se instalarán próximamente. Son proyectos que traerán mayor o menor beneficio a la ciudad, pero que no tienen tantos puntos en contra como la mina de Litio.

### La mina está demasiado cerca de la ciudad y del pantano del que bebemos
Mi postura sería muy diferente si el yacimiento no se hubiera encontrado tan cerca del nucleo urbano. Si estuviera en mitad de una llanura a 10Km de la ciudad y del pantano, estaría completamente a favor. Pero no es el caso.

Está a dos kilómetros cuesta abajo del Pantano de Valdesalor. Para hacerse una idea, corriendo en línea recta una persona que esté medianamente en forma puede recorrer esa distancia en 10 minutos. Es conocido que este tipo de explotaciones requieren muchísima agua, pero el mayor problema no es solo conseguir el agua sino dónde va a ir a parar ese agua después.

¿Qué pasa si hay un accidente o una filtración y el agua va directa al pantano?

¿Puede alguien asegurar que el polvo que se genere en la extracción no va a terminar en la ciudad o en el pantano?

### No hay marcha atrás
Una vez hecho el agujero, el agujero se quedará ahí. No importa que el consorcio se haya comprometido a dejarlo todo arreglado cuando todo termine dentro de 30 años. Una empresa puede darse en concurso de acreedores y que sus socios se desentiendan de todas las obligaciones siempre que no haya consecuencias penales. No me extrañaría que un año antes de terminar la explotación, la empresa que se ha creado expresamente para explotar esta mina, quebrase y dejara la mina abandonada desentendiéndose completamente de cualquier acción de restauración a la que se hayan comprometido para obtener los permisos.

Me gustaría dejar este artículo abierto, posiblemente genere algo de debate en los comentarios y posiblemente cambie cosas, por lo que espero que me perdonéis si edito algún contenido después de publicado.
