---
layout: post
title:  "Por qué cierro facturacionautonomos"
author: alvaro
categories: [ Personal ]
image: assets/images/closed.jpg
image_credits: https://unsplash.com/photos/iaSzwYccV28
description: "Después de 10 años he decidido cerrar mi side-project. Creo que no es viable y necesito eliminar preocupaciones de mi vida."
featured: false
hidden: false
---

La historia de facturacionautonomos es muy simple, no encontraba una herramienta de facturación con la que hacer mis propias facturas. Así que en unos días en los que había poco trabajo, decidimos junto con el pequeño equipo que éramos entonces en DSmultimedia hacernos una. Gracias a toda la potencia que nos daba drupal fue un trabajo de poco más de una semana. 

En principio iba a ser algo interno. Muchas veces no sabía qué factura habían pagado y cual no, ni tenía un resumen de cuánto había facturado en el trimestre. Todas las herramientas que encontraba estaban hechas para contables y economistas y me parecían bastante complicadas.

Entonces decidimos abrirla para que cualquiera pudiera usarla, total, éramos muy partidarios de la cultura libre así que ya que estaba hecha, que la aprovecharan otros. Securizamos todo con cuidado, dimos de alta los registros en la Agencia de Protección de Datos y nos cuidamos de que nos redactaran unos términos y condiciones. Este fue el primer desembolso gordo que hubo que hacer.

Al empezar a gastar dinero pensamos que posiblemente sería buena idea tener un plan para que, por lo menos, se pagara sola. Ideamos entonces un modelo de negocio basado en tener un acceso premium para asesorías. Este acceso premium les permitiría, a cambio de una cuota anual. Acceder a todas las facturas de sus clientes. "Si les damos los datos ya procesados y les ahorramos unas cuantas horas de trabajo, seguramente estén dispuestos a pagar por ello", pensamos entonces.

En aquellos tiempos empezamos a crecer, nos unimos a otra compañía y finalmente nuestra antigua empresa desapareció. Y me quedé yo personalmente a cargo del desarrollo. En parte porque me seguía pareciendo una idea viable y en parte porque no quería cortarle el servicio a los cientos de usuarios que cada día utilizaban la herramienta.

En el verano de 2018 hice la segunda versión de facturacionautonomos. Me puse de plazo final de año para terminarla. Y a pesar de que casi cumplí el plazo no llegué a tiempo. Y al no llegar a tiempo, decidí fijar el plazo en final de 2019.

Durante 2019 no tuve el tiempo ni las ganas de terminar esa segunda versión, a pesar de que le quedaban apenas 20 o 30 horas de trabajo. Después de invertir más de 300, el último 10% de flecos y retoques me suponía una barrera insalvable. Uno de los aspectos que peor llevaba en era el tener que trabajar completamente solo. Desarrollar software es, en gran parte, diálogo y tener diferentes puntos de vista de cada problema. En este caso yo hacía de cliente, diseñador, UX, gestor de proyecto, frontend, backend y administrador de sistemas. Son demasiados roles para una sola persona.

Compatibilizar un side-project con un trabajo a tiempo completo y una familia es una tarea complicada. Desde ese año 2010 en que todo empezó, me ha dado tiempo a casarme, tener dos hijos y cambiar la perspectiva de las prioridades de mi vida. Ya no quiero ser un emprendedor de éxito. Ahora quiero ser un buen padre, un buen marido y una buena persona. Solo eso, que no es poco.

No quiero tener que irme de vacaciones con el ordenador ni parar en un área de servicio dos horas nunca más porque el servidor se ha caído como me pasó en las vacaciones de verano de 2016.

Hay otro aspecto que me preocupa bastante. Estoy almacenando datos muy sensibles y soy el responsable de ellos. En algún momento me han llegado a insinuar que estoy ganando dinero vendiendo datos de los usuarios y esto me ha hecho reflexionar. A pesar de que soy muy cuidadoso con todo lo relacionado con la seguridad y la privacidad, un error me puede meter en un problema muy serio.

Estoy pagando el servidor, la asesoría legal y el dominio. No es mucho, pero estoy pagando para tener más preocupaciones. Tengo que tener un ojo puesto constantemente en las copias de seguridad, restaurar alguna de vez en cuando para comprobar que se están haciendo bien; Aplicar actualizaciones de seguridad casi todas las semanas y estar pendiente del chat de facebook por si hay algún problema. Llegado este punto me planteo: ¿Merece la pena?

La respuesta es: claramente NO.

Sigo pensando que vender mi tiempo no es el mejor medio de vida que llevar a cabo. Tener un producto del que poder vivir sin tener que vender el tiempo que me queda de vida es, sin lugar a dudas, lo mejor que puedo hacer. Por otro lado hay que saber reconocer cuándo una idea ha fracasado y facturacionautonomos es un fracaso del que espero aprender mucho.

Además de todo esto, llevo aproximadamente un año luchando contra una enfermedad que requiere todavía más de toda mi energía. Sé que voy a ganar esta batalla, pero no puedo permitirme el lujo de dedicar mi tiempo a nada que no me haga feliz.