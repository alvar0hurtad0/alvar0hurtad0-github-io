---
layout: post
title:  "La Jugada de la Cabra"
author: alvaro
categories: [ Personal ]
image: assets/images/la-jugada-de-la-cabra.jpg
image_credits: https://unsplash.com/@strvngefilmss
description: "La realidad depende de nuestra percepción, por eso algun."
featured: false
hidden: false
---
  
Hay momentos en la vida en los que nos encontramos atrapados en situaciones difíciles, sin saber cómo salir de ellas. A 
menudo, buscamos soluciones convencionales a nuestros problemas, pero a veces lo que necesitamos es un enfoque más 
creativo y poco convencional. Esta es la premisa detrás de la historia popular de "La jugada de la cabra".

La historia cuenta cómo un hombre vivía en una casa muy pequeña con su numerosa familia. La falta de espacio en la casa
 llevaba a discusiones y peleas constantes entre los miembros de la familia, y el hombre no sabía cómo resolver el problema. 
 Fue entonces cuando decidió buscar ayuda en un sabio, que le dio un consejo extraño: comprar una cabra y meterla en la casa.

El hombre siguió el consejo del sabio, pero la cabra resultó ser un animal ruidoso y molesto que causaba aún más conflictos
 en la casa. Desesperado, el hombre volvió a ver al sabio para contarle lo que había pasado. Pero en lugar de darle una solución
  inmediata, el sabio le dio la segunda parte de su consejo: sacar la cabra de la casa.

Al día siguiente, el hombre regresó al sabio para agradecerle su consejo. Sin la cabra, la dinámica en la casa había cambiado 
significativamente. Los miembros de la familia habían aprendido a trabajar juntos y a apreciar el espacio limitado, y ya no 
discutían tanto como antes.

La moraleja de la historia es que a veces necesitamos un choque o una solución aparentemente extraña para sacarnos de nuestra zona 
de confort y resolver nuestros problemas. La jugada de la cabra nos enseña que la creatividad y la audacia pueden ser nuestras mejores 
aliadas en momentos de dificultad.

Sin embargo, a mi me gusta sacar otra conclusión de esta historia. Las personas nos adaptamos a lo que tenemos rápidamente. Cuando vivimos
en la abundacia, nos cuesta conformarnos con lo que tenemos y no tardamos en buscar algo más. En cambio, cuando carecemos de lo más
básico, cualquier pequeña mejora en nuestra vida es recibida con alegría.

Creo que es muy importante ser consciente de que esto puede pasarnos a cualquiera: Imagina que tu jefe decide realizar una serie de cambios
que suponen una presión añadida al equipo de trabajo unos meses antes de negociar los aumentos de sueldo y dos o tres semanas antes de que
los aumentos de sueldo se produzcan, decide relajar esos cambios para disminuir un poco la presión y que de esa manera lleguéis a negociar
los aumentos de sueldo con la sensación de que las cosas van a mejor y de esa manera aprovechar el sesgo de Status Quo para que aceptes una
subida de sueldo mínima.

Esto es solo un ejemplo, creo que el poder identificar este tipo de situaciones nos ayuda a evalugar nuestro entorno de una manera más objetiva
y valorar nuestra situación sin buscar consuelo en que podría ser mucho peor.
